# tidbyt apps

Starlark scripts used to make [apps for the tidbyt](https://tidbyt.dev/)

- [defector](./defector.star): this script pulls down the rss feed from [defector.com](https://defector.com) and shows a random article title. I tried generating a QR code but occasionally the URLs would be too long to encode in the 29x29 layout Tidbyt provides. Instead, this also gets the defector logo from the rss feed and displays it to the left of the headline
